#YMeng_share
目前最流行的一个社交分享SDK（友盟），功能强大，还有访问数据统计的功能等，这里提供他官方访问[地址](http://www.umeng.com/).大家有空可以去看看，使用一下！！<br>
这个例子实现的简单效果如下：<br>

![](https://git.oschina.net/JJet/YMeng_share/raw/master/YMeng_Share/YMeng_Share/ScreenShot/QQ20150705-1.png)
![](https://git.oschina.net/JJet/YMeng_share/raw/master/YMeng_Share/YMeng_Share/ScreenShot/QQ20150705-2.png)
![](https://git.oschina.net/JJet/YMeng_share/raw/master/YMeng_Share/YMeng_Share/ScreenShot/QQ20150705-3.png)

## 宝贵意见
如果有任何问题，请到Git@OSC中发消息给我，写上你不明白的地方，我会在Git@OSC给予帮助。[这个是我的GitHub地址](https://github.com/JJetGu)